#!/bin/sh
uvicorn --interface wsgi --workers 2 --host 0.0.0.0 --port 5000 --timeout-keep-alive 5 --reload --reload-dir app app.wsgi:app
FROM python:3.8

COPY requirements.txt /

RUN pip install -r /requirements.txt

COPY . /app

WORKDIR /app

ENTRYPOINT ["./uvicorn_starter.sh"]

#RUN uvicorn --interface wsgi --workers 2 --host 0.0.0.0 --port 5000 --timeout-keep-alive 5 --reload --reload-dir app app.wsgi:app

#CMD [ "pip", "run", "uvicorn", "--interface", "wsgi", "--workers", "2", "--host", "0.0.0.0", "--port", "5000", "--timeout-keep-alive", "5", "--reload", "--reload-dir", "app", "app.wsgi:app" ]

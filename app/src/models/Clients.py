from app.src import db, ma
from datetime import datetime


class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    lastname = db.Column(db.String(80))
    firstname = db.Column(db.String(80))
    email = db.Column(db.String(80))
    date_created = db.Column('date_created', db.DateTime)

    def __init__(self, lastname: str, firstname: str, email: str):
        """On initialise les paramètres de la table du Client

        :param lastname: lastname
        :type lastname: str
        :param firstname: firstname
        :type firstname: str
        :param email: email address
        :type email: str
        """
        self.lastname = lastname
        self.firstname = firstname
        self.email = email
        self.date_created = datetime.utcnow()


class ClientSchema(ma.Schema):
    class Meta:
        fields = ("id", "lastname", "firstname", "email", "date_created")


client_schema = ClientSchema()
clients_schema = ClientSchema(many=True)

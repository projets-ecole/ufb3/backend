from app.src import db, ma


class Produit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    stock = db.Column(db.Integer)
    image = db.Column(db.String(300))
    price = db.Column(db.Integer)

    def __init__(self, name, stock, image, price):
        """On initialise les paramètres de la table du Produits"""
        self.name = name
        self.stock = stock
        self.image = image
        self.price = price


class ProduitSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "stock", "image", "price")


produit_schema = ProduitSchema()
produits_schema = ProduitSchema(many=True)

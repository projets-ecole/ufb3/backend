from app.src import db, ma
from datetime import datetime


class Facture(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    is_pay = db.Column(db.Boolean, default=False)
    price = db.Column(db.Integer)
    pay_date = db.Column(db.DateTime)
    date_created = db.Column(db.DateTime, default=datetime.now)
    id_client = db.Column(db.Integer, db.ForeignKey('client.id'))
    id_produit = db.Column(db.Integer, db.ForeignKey('produit.id'))

    def __init__(self, is_pay, pay_date, price, id_client, id_produit):
        """On initialise les paramètres de la table du Factures"""
        self.is_pay = is_pay
        self.price = price
        self.pay_date = pay_date
        self.date_created = datetime.utcnow()
        self.id_client = id_client
        self.id_produit = id_produit


class FactureSchema(ma.Schema):
    class Meta:
        fields = ("id", "is_pay", "price", "pay_date", "date_created", "id_client", "id_produit")


facture_schema = FactureSchema()
factures_schema = FactureSchema(many=True)

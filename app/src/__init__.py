from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api

app = Flask(__name__)
app.config['DEBUG'] = True
app.config["ENV"] = "development"
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@mysql/ufb3'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_POOL_SIZE"] = 20
app.config["SQLALCHEMY_POOL_TIMEOUT"] = 300


db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

from app.src.routes.Clients import ClientListResource, ClientResource, ClientStructureResource
from app.src.routes.Factures import FactureListResource, FactureResource, FactureStructureResource
from app.src.routes.Produits import ProduitListResource, ProduitResource, ProduitStructureResource

api.add_resource(ClientListResource, '/clients')
api.add_resource(ClientStructureResource, '/clients/structure')
api.add_resource(ClientResource, '/clients/<int:id>')

api.add_resource(FactureListResource, '/factures')
api.add_resource(FactureStructureResource, '/factures/structure')
api.add_resource(FactureResource, '/factures/<int:id>')

api.add_resource(ProduitListResource, '/produits')
api.add_resource(ProduitStructureResource, '/produits/structure')
api.add_resource(ProduitResource, '/produits/<int:id>')


CORS(app)

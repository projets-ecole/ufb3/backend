from app.src import db
from app.src.models.Produits import Produit, produit_schema, produits_schema

from flask import request
from flask_restful import Resource


class ProduitListResource(Resource):
    def get(self):
        produits = Produit.query.order_by(Produit.stock).all()
        return produits_schema.dump(produits)

    def post(self):
        new_produit = Produit(
            name=request.json['name'],
            stock=request.json['stock'],
            image=request.json['image'],
            price=request.json['price']
        )
        db.session.add(new_produit)
        db.session.commit()
        return produit_schema.dump(new_produit)


class ProduitStructureResource(Resource):
    def get(self):
        data = {
                'id': 'number',
                'name': 'text',
                'stock': 'number',
                'image': 'text',
                'price': 'number'
            }
        return data


class ProduitResource(Resource):
    def get(self, id):
        produit = Produit.query.get_or_404(id)
        return produit_schema.dump(produit)

    def patch(self, id):
        produit = Produit.query.get_or_404(id)
        if 'name' in request.json:
            produit.name = request.json['name']
        if 'stock' in request.json:
            produit.stock = request.json['stock']
        if 'image' in request.json:
            produit.image = request.json['image']
        if 'price' in request.json:
            produit.price = request.json['price']

        db.session.commit()
        return produit_schema.dump(produit)

    def delete(self, id):
        produit = Produit.query.get_or_404(id)
        db.session.delete(produit)
        db.session.commit()
        return produit_schema.dump(produit)

from app.src import db
from app.src.models.Factures import Facture, facture_schema, factures_schema

from flask import request
from flask_restful import Resource


class FactureListResource(Resource):
    def get(self):
        facture = Facture.query.order_by(Facture.date_created).all()
        return factures_schema.dump(facture)

    def post(self):
        new_facture = Facture(
            is_pay=False,
            price=request.json['price'],
            pay_date=request.json['pay_date'],
            id_client=request.json['id_client'],
            id_produit=request.json['id_produit']
        )
        db.session.add(new_facture)
        db.session.commit()
        return facture_schema.dump(new_facture)


class FactureStructureResource(Resource):
    def get(self):
        data = {
                'id': 'number',
                'is_pay': 'checkbox',
                'price': 'number',
                'pay_date': 'date',
                'date_created': 'date',
                'id_client': 'number',
                'id_produit': 'number',
            }
        return data


class FactureResource(Resource):
    def get(self, id):
        facture = Facture.query.get_or_404(id)
        return facture_schema.dump(facture)

    def patch(self, id):
        facture = Facture.query.get_or_404(id)
        if 'is_pay' in request.json:
            facture.is_pay = True
        if 'price' in request.json:
            facture.price = request.json['price']
        if 'pay_date' in request.json:
            facture.pay_date = request.json['pay_date']
        if 'id_produit ' in request.json:
            facture.id_produit = request.json['id_produit']
        if 'id_client ' in request.json:
            facture.id_client = request.json['id_client']

        db.session.commit()
        return facture_schema.dump(facture)

    def delete(self, id):
        facture = Facture.query.get_or_404(id)
        db.session.delete(facture)
        db.session.commit()
        return facture_schema.dump(facture)

import datetime

from app.src import db
from app.src.models.Clients import Client, client_schema, clients_schema

from flask import request
from flask_restful import Resource


class ClientListResource(Resource):
    def get(self):
        clients = Client.query.order_by(Client.date_created).all()
        return clients_schema.dump(clients)

    def post(self):
        new_client = Client(
            lastname=request.json['lastname'],
            firstname=request.json['firstname'],
            email=request.json['email']
        )
        db.session.add(new_client)
        db.session.commit()
        return client_schema.dump(new_client)


class ClientStructureResource(Resource):
    def get(self):
        data = {
                'lastname': 'text',
                'firstname': 'text',
                'email': 'email',
        }
        return data


class ClientResource(Resource):
    def get(self, id):
        client = Client.query.get_or_404(id)
        return client_schema.dump(client)

    def patch(self, id):
        client = Client.query.get_or_404(id)
        if 'lastname' in request.json:
            client.lastname = request.json['lastname']
        if 'firstname' in request.json:
            client.firstname = request.json['firstname']
        if 'email' in request.json:
            client.email = request.json['email']

        db.session.commit()
        return client_schema.dump(client)

    def delete(self, id):
        client = Client.query.get_or_404(id)
        db.session.delete(client)
        db.session.commit()
        return client_schema.dump(client)
